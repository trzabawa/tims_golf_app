<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_85898c65d1d36f8710a4ece0cbf11e8a8d57e877e6510d84ad87f7c0e6b7f973 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_88364b1898b094a51e5de39530d019c2251c05a326f6bc9a03841c54ea9071a6 = $this->env->getExtension("native_profiler");
        $__internal_88364b1898b094a51e5de39530d019c2251c05a326f6bc9a03841c54ea9071a6->enter($__internal_88364b1898b094a51e5de39530d019c2251c05a326f6bc9a03841c54ea9071a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_88364b1898b094a51e5de39530d019c2251c05a326f6bc9a03841c54ea9071a6->leave($__internal_88364b1898b094a51e5de39530d019c2251c05a326f6bc9a03841c54ea9071a6_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e68c89b4446a4e08cd3e8ff2ab6671a819755f8e84cc6b133c5715cb5ff77d14 = $this->env->getExtension("native_profiler");
        $__internal_e68c89b4446a4e08cd3e8ff2ab6671a819755f8e84cc6b133c5715cb5ff77d14->enter($__internal_e68c89b4446a4e08cd3e8ff2ab6671a819755f8e84cc6b133c5715cb5ff77d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_e68c89b4446a4e08cd3e8ff2ab6671a819755f8e84cc6b133c5715cb5ff77d14->leave($__internal_e68c89b4446a4e08cd3e8ff2ab6671a819755f8e84cc6b133c5715cb5ff77d14_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_fea37386cd10b2a268fc33f5d18ba5ed00a99f3932d5942bf3535600c707bc46 = $this->env->getExtension("native_profiler");
        $__internal_fea37386cd10b2a268fc33f5d18ba5ed00a99f3932d5942bf3535600c707bc46->enter($__internal_fea37386cd10b2a268fc33f5d18ba5ed00a99f3932d5942bf3535600c707bc46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_fea37386cd10b2a268fc33f5d18ba5ed00a99f3932d5942bf3535600c707bc46->leave($__internal_fea37386cd10b2a268fc33f5d18ba5ed00a99f3932d5942bf3535600c707bc46_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_3c345a95ebf2ee25e1a6a0ea4943d640baee3eda2a7d1df04c4395341cc0aa47 = $this->env->getExtension("native_profiler");
        $__internal_3c345a95ebf2ee25e1a6a0ea4943d640baee3eda2a7d1df04c4395341cc0aa47->enter($__internal_3c345a95ebf2ee25e1a6a0ea4943d640baee3eda2a7d1df04c4395341cc0aa47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_3c345a95ebf2ee25e1a6a0ea4943d640baee3eda2a7d1df04c4395341cc0aa47->leave($__internal_3c345a95ebf2ee25e1a6a0ea4943d640baee3eda2a7d1df04c4395341cc0aa47_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
