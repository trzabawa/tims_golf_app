<?php

/* golfBundle:Default:index2.html.twig */
class __TwigTemplate_32c94544f897c17f86e14e7ab8994868f84c155df518e01e9496b519291bc54d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fb68e17b802e8fceb51fc7285a1007f4c8c60a9fa4e236c79263a21afdd2220 = $this->env->getExtension("native_profiler");
        $__internal_3fb68e17b802e8fceb51fc7285a1007f4c8c60a9fa4e236c79263a21afdd2220->enter($__internal_3fb68e17b802e8fceb51fc7285a1007f4c8c60a9fa4e236c79263a21afdd2220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:Default:index2.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" href=\"animate.css\">
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
    <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' 
type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <script type='text/javascript' src=\"http://malsup.github.io/jquery.cycle.all.js\"></script>

<style>


#slider {
position: absolute;
width: 30vw;
height: 42vh;
overflow: hidden;
left: 35vw;
top: 25vh;
z-index: 100;
border-radius: 5px;
}

.info {
position: relative;
width: 30vw;
height: 30vh;
}

#header_table {
border: 1px solid black;
width: 100%;
background: #CADDE4;
height: 20%;
}

#header_table h1 {
font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;    
margin-top: 5px;
margin-bottom: 5px;
margin-left: 5px;
color: #505050;\t
}

#scorecard_table {
border: 1px solid black;
width: 100%;
height: 80%;
}

.scorecard_heading {
border: 1px solid black;
padding: 5px 5x 5px 5px;
text-align: center;
}

.blue {
background: #406424;
color: white;
}

.green {
background: #FFFFFF;
color: #505050
}

.cream {
background: #CADDE4;
color: #505050;
}

#scorecard_table .scorecard_heading h1 {
font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;    
margin-top: 5px;
margin-bottom: 5px;
}

#scorecard_table div {
font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;    
}

#next {
width: 50px;
height: 50px;
line-height: 50px;
text-align: center;
position: absolute;
top: 45vh;
left: 72vw;
background: black;
color: white;
cursor: pointer;
z-index: 999;
font-size: 25px;
border-radius: 50%;
}

#prev {
width: 50px;
height: 50px;
line-height: 50px;
text-align: center;
position: absolute;
top: 45vh;
left: 25VW;
background: black;
color: white;
cursor: pointer;
z-index: 999;
font-size: 25px;
border-radius: 50%;
}

#pager {
width: 100%;
text-align: center;
position: absolute;
cursor: pointer;
z-index: 999;
top: 70vh;
}

#pager a {
width: 20px;
height: 20px;
display: inline-block;
position: relative;
border: 1px solid black;
border-radius: 20px;
background: transparent;
margin: 10px;
font-size: .0em;
color: transparent;
}

#pager a.activeSlide {
background: black;
}

#container {
    position: relative;
    }

header {
    position: fixed;
    width: 100%;
    z-index: 1;
    }

#nav_bar {
    background: #505050;
    padding: 15px;
    }

#nav_bar li a {
    color: white;
    font: 18px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#nav_bar a:hover {
    color: #0CA0C6;
    }

#golf_tracker {
    color: white;
    font-size: 35px;
    font-family: 'Lobster', cursive;
    padding-top: 15px;
    }

#username {
    color: white;
    font: 18px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    margin-left: 10px;
    }

#golf_course_photo {
    position: absolute;
    left: 5vw;
    width: 90vw;
    height: 100vh;
}

ul {
padding: 0;
}

#golf_average_and_handicap {
    position: fixed;
    z-index: 10;
    left: 40vw;
    top: 80vh;  
}

.average_handicap_score {
    background: #CADDE4;
    color: #505050;
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 20px 10px 20px 10px;
    display: inline-block;
    border-radius: 5px;
    text-align: center;
    width: 10vw;
       
}

@media screen and (min-width: 1000px) and (max-width: 1150px) {
    #slider {
    width: 40vw;
    left: 30vw;
   }

   .info {
   width: 40vw;
   }

   #next {
   top: 40vh;
   left: 75vw;
   }

   #prev {
   top: 40vh;
   left: 20vw;
   }

   #pager {
   top: 65vh;
   }
    
   #golf_average_and_handicap {
   left: 35vw;
   } 
   .average_handicap_score {
   width: 15vw;
   } 

}

@media screen and (max-width: 1000px){
    #golf_tracker {
    font-size: 25px;
    padding-left: 0px;
    }

    #username {
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 15px 0px 0px 0px;
    }

    #golf_course_photo {
    height: 100%;
    width: 100%;
    left: 0%;
    }

    #average {
    font: 20px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

    #handicap {
    font: 20px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    margin-left: 0px;

    }
  
}
</style>

<title>TestPage</title>

</head>

<body>

<div id-=\"container\">

<div id=\"slider\">

<div class=\"info\">
<table id=\"header_table\">
\t
<tr>
<td>
<h1><b>Course: Maple Lane Golf Course</b></h1>
<h1><b>Date:<b></h1>
</td>
</tr>
</table>

<table id=\"scorecard_table\">
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>1</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>2</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>6</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>7</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>8</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>9</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par</b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>10</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>11</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>12</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>13</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>14</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>15</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>16</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>17</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>18</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par<b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>34</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>34</b></div>
</td>
</tr>
</table>\t
</div>


<div class=\"info\">
<table id=\"header_table\">
\t
<tr>
<td>
<h1><b>Course: Maple Lane Golf Course</b></h1>
<h1><b>Date:<b></h1>
</td>
</tr>
</table>

<table id=\"scorecard_table\">
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>1</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>2</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>6</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>7</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>8</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>9</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par</b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>10</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>11</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>12</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>13</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>14</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>15</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>16</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>17</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>18</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par<b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>34</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>34</b></div>
</td>
</tr>
</table>\t
</div>

<div class=\"info\">
<table id=\"header_table\">
\t
<tr>
<td>
<h1><b>Course: Maple Lane Golf Course</b></h1>
<h1><b>Date:<b></h1>
</td>
</tr>
</table>

<table id=\"scorecard_table\">
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>1</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>2</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>6</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>7</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>8</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>9</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par</b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>10</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>11</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>12</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>13</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>14</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>15</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>16</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>17</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>18</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par<b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>34</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>34</b></div>
</td>
</tr>
</table>\t
</div>

<div class=\"info\">
<table id=\"header_table\">
\t
<tr>
<td>
<h1><b>Course: Maple Lane Golf Course</b></h1>
<h1><b>Date:<b></h1>
</td>
</tr>
</table>

<table id=\"scorecard_table\">
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>1</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>2</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>6</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>7</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>8</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>9</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par</b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>10</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>11</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>12</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>13</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>14</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>15</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>16</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>17</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>18</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par<b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>34</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>34</b></div>
</td>
</tr>
</table>\t
</div>

<div class=\"info\">
<table id=\"header_table\">
\t
<tr>
<td>
<h1><b>Course: Maple Lane Golf Course</b></h1>
<h1><b>Date:<b></h1>
</td>
</tr>
</table>

<table id=\"scorecard_table\">
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>1</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>2</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>6</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>7</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>8</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>9</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par</b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>36</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading blue\">
\t<h1><b>Hole</b></h1>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>10</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>11</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>12</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>13</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>14</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>15</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>16</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>17</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<div><b>18</b></div>
</td>
<td class=\"scorecard_heading blue\">
\t<h1><b>Total</b></h1>
</td>
</tr>
<tr>
<td class=\"scorecard_heading green\">
\t<h1><b>Par<b></h1>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>4</b></div>
</td>
</td>
<td class=\"scorecard_heading green\">
\t<div><b>34</b></div>
</td>
</tr>
<tr>
<td class=\"scorecard_heading cream\">
\t<h1><b>Score</b></h1>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>3</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>5</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>4</b></div>
</td>
<td class=\"scorecard_heading cream\">
\t<div><b>34</b></div>
</td>
</tr>
</table>\t
</div>
</div>

<div id=\"pager\"></div>
<div id=\"next\">></div>
<div id=\"prev\"><</div>


<header>
<nav id=\"nav_bar\" class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-
example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a id=\"golf_tracker\" class=\"navbar-brand\" href=\"#\">Golf<b>Tracker</b></a>
      <a id=\"username\" class=\"navbar-brand\" href=\"#\">";
        // line 1436
        echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : $this->getContext($context, "username")), "html", null, true);
        echo "</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\"></ul>
      <ul class=\"nav navbar-nav navbar-right\">
\t<li><a id=\"sign_in\" href=\"#\">Search Round</a>
        <li><a href=\"#\">Enter Round</a>
        <li><a href=\"";
        // line 1444
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\">Sign Out</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>

<img id=\"golf_course_photo\" 
src=\"http://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/TROU_12.jpg/1024px-TROU_12.jpg\"/>

<div id=\"golf_average_and_handicap\">
<ul>
    <li class=\"average_handicap_score\"><div><b> Golf Average</b></div><div><b>90.5</b></div></li>
    <li class=\"average_handicap_score\"><div><b>Golf Handicap</b></div><div><b>14</b></div></li>
</ul>
</div>

</div>

</body>

<script type=\"text/javascript\">

\$(\"#slider\").cycle({
\t
\tfx:\t'scrollHorz',
\tnext:\t'#next',
\tprev:\t'#prev',
\tpager:\t'#pager',
\ttimeout:      0,
\t});

</script>

</html>



";
        
        $__internal_3fb68e17b802e8fceb51fc7285a1007f4c8c60a9fa4e236c79263a21afdd2220->leave($__internal_3fb68e17b802e8fceb51fc7285a1007f4c8c60a9fa4e236c79263a21afdd2220_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:Default:index2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1470 => 1444,  1459 => 1436,  22 => 1,);
    }
}
