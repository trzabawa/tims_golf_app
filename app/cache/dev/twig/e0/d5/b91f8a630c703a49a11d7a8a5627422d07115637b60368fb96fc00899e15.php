<?php

/* golfBundle:Default:index.html.twig */
class __TwigTemplate_e0d5b91f8a630c703a49a11d7a8a5627422d07115637b60368fb96fc00899e15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a61c1668a189f3a0b65d1527ae3af62aacc6b96ee5fdafde2ec1b1a72dfd9d83 = $this->env->getExtension("native_profiler");
        $__internal_a61c1668a189f3a0b65d1527ae3af62aacc6b96ee5fdafde2ec1b1a72dfd9d83->enter($__internal_a61c1668a189f3a0b65d1527ae3af62aacc6b96ee5fdafde2ec1b1a72dfd9d83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:Default:index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"></script>
    <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

<style>

#view_height {
    height: 100vh;
    }

header {
    position: fixed;
    width: 100%;
    z-index: 10;
    }

#nav_bar {
    background: #505050;
    padding: 15px;
    }

#nav_bar li a {
    color: white;
    font: 18px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#nav_bar a:hover {
    color: #0CA0C6;
    }

#golf_tracker {
    color: white;
    font-size: 35px;
    font-family: 'Lobster', cursive;
    padding-top: 15px;
    }



#banner {
    top: 90px;
    position: fixed;
}

#golf_photo {
    width: 80%;
    border-radius: 5px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    }

#content {
    margin: auto;
    position: relative;
    top: 700px;
    color: #505050;
    width: 80%;
    border: 1px solid #fff;
    border-radius: 1px;
    background: #f6f6f6;
    box-shadow: 0 0 2px rgba(50,50,50,0.5);
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 5px;
    text-align: center;
    }

#content p {
    text-align: left;
    }

#content_symbols {
    font-size: 25px;
    color: #0CA0C6;
    }

.content_headings {
    font-size: 30px;
    margin-top: 25px;
    }

#get_started {
    margin-top: 450px;
    margin-left: 800px;
    position: fixed;
    color: white;
    font-family: 'Lobster', cursive;
    text-align: center;
    }

#get_started_button {
    font: 20px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    border-radius: 10px;
    margin-top: 10px;
    box-shadow: 0 0 25px white;
    -o-box-shadow: 0 0 25px white;
    -moz-box-shadow: 0 0 25px white;
    -webkit-box-shadow: 0 0 25px white;
    -ms-box-shadow: 0 0 25px white;
    background: #0CA0C6;
    }

.whitespace {
    top: 700px;
    position: relative;
    width: 100%;
    height: 110px;
    background: white;
    }

#features {
    margin: auto;
    position: relative;
    top: 700px;
    color: #505050;
    width: 80%;
    border: 1px solid #fff;
    border-radius: 1px;
    background: #f6f6f6;
    box-shadow: 0 0 2px rgba(50,50,50,0.5);
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 5px;
    text-align: center;
    }

#features_symbols {
    font-size: 25px;
    color: #0CA0C6;
    }

#features div h1 {
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#footer {
    top: 700px;
    position: relative;
    width: 100%;
    height: 100px;
    background: #505050;
    border-radius:
    }

#footer_content {
    list-style-type: none;
    text-align: center;
    padding-top: 30px;
    padding-left: 0px;

    }

#footer_content li {
    display: inline;
    }

.footer_info {
        font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
        color: white;
    }

#footer li a:hover {
    color: #0CA0C6;
    text-decoration: none;
    }
#overlay {
    position: fixed;
    width: 100%;
    height: 100%;
    background: #000;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    opacity: 0.8;
    z-index: 1000;
    display: none;
    }

    .table {
        text-align: center;
        margin: 0 auto;
        font: 20px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
        color: #505050;
        }

    #overlay_signIn_form {
        width: 30%;
        height: 50%;
        margin: 0 auto;
        position: absolute;
        top: 20%;
        right: 20%;
        bottom: 20%;
        left: 20%;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;
    }

    #overlay_register_form {
        width: 30%;
        height: 50%;
        margin: 0 auto;
        position: absolute;
        top: 20%;
        right: 20%;
        bottom: 20%;
        left: 20%;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;
    }

    .logIn {
        font-size: 50px;
        font-family: 'Lobster', cursive;
        text-shadow: 0 0 50px white;
        -o-text-shadow: 0 0 50px white;
        -moz-text-shadow: 0 0 50px white;
        -webkit-text-shadow: 0 0 50px white;
        -ms-text-shadow: 0 0 50px white;
    }

    .overlay_exit_button {
        width: 30px;
        line-height: 30px;
        background: #505050;
        color: white;
        text-align: center;
        font-size: 16px;
        font-weight: bold;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        position: absolute;
        top: -10px;
        right: -10px;
    }

    #submit {
    background: white;
    border-radius: 10px;
    }

@media screen and (min-width: 1000px) and (max-width: 1150px) {

    #get_started {
        margin-top: 600px;
        margin-left: 525px;
        position: fixed;
        color: white;
        font-family: 'Lobster', cursive;
        text-align: center;
    }

    #golf_photo {
        height: 700px;
    }
}

@media screen and (max-width: 1000px){
  #golf_photo {
    width: 100%;
  }

  #content {
    width: 100%;
  }

  #features {
    width: 100%;
  }

  #footer_content {
        padding-top: 20px;
    }
  #footer_content li {
        padding: 0px;
    }

  .whitespace {
        height: 60px;
    }

  #get_started {
    margin-top: 100%;
    margin-left: 20px;
    position: fixed;
    color: #5F7B31;
    font-family: 'Lobster', cursive;
    }

  #get_started_button {
    font: 20px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    border-radius: 5px;
    margin-top: 50px;
    box-shadow: 0 0 25px;
    -o-box-shadow: 0 0 25px;
    -moz-box-shadow: 0 0 25px;
    -webkit-box-shadow: 0 0 25px;
    -ms-box-shadow: 0 0 25px;
    background: #0CA0C6;
    }

    #overlay_signIn_form {
        width: 100%;
        height: 50%;
        left: 0px;
        right: 0px;
    }

    #overlay_register_form {
        width: 100%;
        height: 50%;
        left: 0px;
        right: 0px;
    }


    .overlay_exit_button {
        top: -5px;
        right: 0px;
    }
}



</style>

<title>GolfTracker</title>

</head>

<body>

<div id=\"overlay\"></div>

<div id=\"overlay_signIn_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">SignIn</h1>
                    <form action=\"/LogIn\" method=\"POST\">
                        Username:<br>
                        <input type=\"text\" name=\"username\">
                        <br>
                        Password:<br>
                        <input type=\"password\" name=\"password\">
                        <br><br>
                        <input id=\"submit\" type=\"submit\" value=\"Submit\">
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id=\"overlay_register_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">Register</h1>
                    <form action=\"/LogIn\" method=\"POST\">
                        New Username:<br>
                        <input type=\"text\" name=\"username\">
                        <br>
                        New Password:<br>
                        <input type=\"password\" name=\"password\">
                        <br><br>
                        <input id=\"submit\" type=\"submit\" value=\"Submit\">
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id-=\"container\">

<div id=\"view_height\">
<header>
<nav id=\"nav_bar\" class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a id=\"golf_tracker\" class=\"navbar-brand\" href=\"#\">Golf<b>Tracker</b></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
        <li><a id=\"point_1\" href=\"#\">How It Works</a></li>
        <li><a id=\"point_2\" href=\"#\">Features</a></li>
      </ul>
      <ul class=\"nav navbar-nav navbar-right\">
        <li><a id=\"sign_in\" href=\"#\">Sign In</a></li>
        <li><a id=\"register\" href=\"#\">Register</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>

<div id=\"banner\">
    <img id=\"golf_photo\" src=\"https://farm8.staticflickr.com/7367/9321187216_13a434cf04_o_d.jpg\"/>
</div>

<table id=\"get_started\">
    <tr>
        <td>
            <h1>Golf Tracking Made Simple</h1>
        </td>
    </tr>
    <tr>
        <td>
            <button id=\"get_started_button\" type=\"button\" class=\"btn btn-info\">Get Started</button>
        </td>
    </tr>
</table>

</div>
<div id=\"1\" class=\"whitespace\"></div>

<div id=\"content\">
    <h1>How It Works</h1>
    <div class=\"row Grid1\">
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Get started.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-refresh\" aria-hidden=\"true\"></span>
            <p>After you create your account, you will be redirected to your home page. You will see 4 fields:</p>
            <p>1). Your average.</p>
            <p>2). Your handicap.</p>
            <p>3). The last 5 courses you played at and the scores you recorded.</p>
            <p>4). A button for entering a new score.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Search course.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span>
            <p>Once you have clicked on a button to enter a new score, you will be taken to a page where it will first require you to enter the course your playing it.</p>
            <p>If the course hasn't been entered previously, you will have to fill out three fields:</p>
            <p>1) Course name.</p>
            <p>2) Course city.</p>
            <p>3) Course state.</p>
            <p><b>*</b>Remember that you only have to enter a new course once.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Enter score.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>
            <p>Upon entering a new course or searching for one already existing, you will now be taken to the score page.</p>
            <p>Here will be able to enter your round. Notice that if the course already exists, you should see the par of every hole.</p>
            <p>If not, then congratulations, your the first player of this course and you will do the par inputting your self! The process of filling out your scorecard is pretty easy from here.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Have fun.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\"></span>
            <p>After your enter your round, you will be taken back to your home page.</p>
            <p>Behind the scenes we will be taking care of comprising your average, handicap, and displaying the data on the screen. Have fun and good luck!
        </div>
    </div>
</div>

<div class=\"whitespace\"></div>

<div id=\"features\">
<h1 id=\"features_heading\">Features</h1>
<div class=\"row Grid1\">
        <div class=\"col-xs-12 col-md-4\">
            <h1>Automated tracking of your golf average.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-signal\" aria-hidden=\"true\"></span>
        </div>
        <div class=\"col-xs-12 col-md-4\">
            <h1>Automated tracking of your handicap.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-plus-sign\" aria-hidden=\"true\"></span>
        </div>
        <div class=\"col-xs-12 col-md-4\">
            <h1>List of the 5 recent golf courses you played at and the scores you recorded.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></span>
        </div>
</div>
</div>

<div id=\"2\" class=\"whitespace\"></div>

<footer>
    <div id=\"footer\">
        <ul id=\"footer_content\">
            <div class=\"row Grid1\">
                <div class=\"col-xs-12 col-md-6\">
                    <li><a class=\"footer_info\" href=\"#\">Copyright © GolfTracker. All rights reserved.</a></li>
                </div>
                <div class=\"col-xs-12 col-md-6\">
                    <li><a class=\"footer_info\" href=\"#\">Terms and Conditions of Use & Disclaimer.</a></li>
                </div>
            </div>
        </ul>
    </div>
</footer>

</div>
</body>

<script type=\"text/javascript\">
\$(function() {

    \$(\"#point_1\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":\$(\"#1\").offset().top}, 1000);
    return false;
    });

    \$(\"#point_2\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":\$(\"#2\").offset().top}, 1000);
    return false;
    });

    \$(\"#golf_tracker\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":0}, 1000);
    return false;
    });

    \$(\"#sign_in\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_signIn_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\"#register\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_register_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\".overlay_exit_button\").on(\"click\", function () {
    \$(\"#overlay\").fadeOut(\"fast\");
    \$(\"#overlay_signIn_form\").fadeOut(\"fast\");
    \$(\"#overlay_register_form\").fadeOut(\"fast\");
    \$(\".overlay_exit_button\").fadeOut(\"fast\");
    });
});

</script>

</html>




";
        
        $__internal_a61c1668a189f3a0b65d1527ae3af62aacc6b96ee5fdafde2ec1b1a72dfd9d83->leave($__internal_a61c1668a189f3a0b65d1527ae3af62aacc6b96ee5fdafde2ec1b1a72dfd9d83_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:Default:index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
