<?php

/* golfBundle:Default:testpage.html.twig */
class __TwigTemplate_33f008858b1b553610d02d91bed4eb5e1dff71c658893044c286d62ef0f419e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dadb68f0f41586ad8ca636456306681809e2cef84ad0b0a3033c09d4f2d63872 = $this->env->getExtension("native_profiler");
        $__internal_dadb68f0f41586ad8ca636456306681809e2cef84ad0b0a3033c09d4f2d63872->enter($__internal_dadb68f0f41586ad8ca636456306681809e2cef84ad0b0a3033c09d4f2d63872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:Default:testpage.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" href=\"animate.css\">
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
    <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' 
type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/animate.css"), "html", null, true);
        echo "\">

<style>
</style>

<title>TestPage</title>

</head>

<body>
    <h1>Test Successfull!</h1>
</body>

</html>";
        
        $__internal_dadb68f0f41586ad8ca636456306681809e2cef84ad0b0a3033c09d4f2d63872->leave($__internal_dadb68f0f41586ad8ca636456306681809e2cef84ad0b0a3033c09d4f2d63872_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:Default:testpage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 14,  22 => 1,);
    }
}
