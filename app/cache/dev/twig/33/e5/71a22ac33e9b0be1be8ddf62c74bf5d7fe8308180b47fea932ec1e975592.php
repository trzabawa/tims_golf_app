<?php

/* golfBundle:default:form2saved.html.twig */
class __TwigTemplate_33e571a22ac33e9b0be1be8ddf62c74bf5d7fe8308180b47fea932ec1e975592 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41d625044d9cc564451359e8e7af45c64725c3da928e5932347d695dedd40b9a = $this->env->getExtension("native_profiler");
        $__internal_41d625044d9cc564451359e8e7af45c64725c3da928e5932347d695dedd40b9a->enter($__internal_41d625044d9cc564451359e8e7af45c64725c3da928e5932347d695dedd40b9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:default:form2saved.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <title>form2saved</title>
</head>
<body>
";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "message"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "        <p>";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo " 
</body>
</html>




";
        
        $__internal_41d625044d9cc564451359e8e7af45c64725c3da928e5932347d695dedd40b9a->leave($__internal_41d625044d9cc564451359e8e7af45c64725c3da928e5932347d695dedd40b9a_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:default:form2saved.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 11,  35 => 9,  31 => 8,  22 => 1,);
    }
}
