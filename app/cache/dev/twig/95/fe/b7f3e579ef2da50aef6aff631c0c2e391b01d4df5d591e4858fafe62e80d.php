<?php

/* golfBundle:default:form2.html.twig */
class __TwigTemplate_95feb7f3e579ef2da50aef6aff631c0c2e391b01d4df5d591e4858fafe62e80d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebbb5cd7ea6983e50ba7fac602f8aa3c75e6ee5af7f9c12b3c51d38ab00a80bb = $this->env->getExtension("native_profiler");
        $__internal_ebbb5cd7ea6983e50ba7fac602f8aa3c75e6ee5af7f9c12b3c51d38ab00a80bb->enter($__internal_ebbb5cd7ea6983e50ba7fac602f8aa3c75e6ee5af7f9c12b3c51d38ab00a80bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:default:form2.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <title>form2</title>
</head>
<body>
";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
</body>
</html>";
        
        $__internal_ebbb5cd7ea6983e50ba7fac602f8aa3c75e6ee5af7f9c12b3c51d38ab00a80bb->leave($__internal_ebbb5cd7ea6983e50ba7fac602f8aa3c75e6ee5af7f9c12b3c51d38ab00a80bb_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:default:form2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  22 => 1,);
    }
}
