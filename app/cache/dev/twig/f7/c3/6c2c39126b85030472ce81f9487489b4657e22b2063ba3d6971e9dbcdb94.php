<?php

/* golfBundle:Default:sent.html.twig */
class __TwigTemplate_f7c36c2c39126b85030472ce81f9487489b4657e22b2063ba3d6971e9dbcdb94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f2b2a36091e8db8b6aa3c0a291dd47e8aee9b40589e765acd7651c85391652c = $this->env->getExtension("native_profiler");
        $__internal_2f2b2a36091e8db8b6aa3c0a291dd47e8aee9b40589e765acd7651c85391652c->enter($__internal_2f2b2a36091e8db8b6aa3c0a291dd47e8aee9b40589e765acd7651c85391652c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:Default:sent.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frameworks/css/bootstrap.min.css"), "html", null, true);
        echo "\">
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frameworks/javascript/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frameworks/javascript/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

<style>

#view_height {
    height: 100vh;
    }

header {
    position: fixed;
    width: 100%;
    z-index: 10;
    }

#nav_bar {
    background: #505050;
    padding: 15px;
    }

#nav_bar li a {
    color: white;
    font: 18px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#nav_bar a:hover {
    color: #0CA0C6;
    }

#golf_tracker {
    color: white;
    font-size: 35px;
    font-family: 'Lobster', cursive;
    padding-top: 15px;
    }

#banner {
    width: 100%;
    position: fixed;
}

#golf_photo {
    width: 90vw;
    height: 100vh;
    border-radius: 5px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    }

#content {
    margin: auto;
    position: relative;
    top: 100vh;
    color: #505050;
    width: 90vw;
    border: 1px solid #fff;
    border-radius: 1px;
    background: #f6f6f6;
    box-shadow: 0 0 2px rgba(50,50,50,0.5);
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 5px;
    text-align: center;
    }

#content p {
    text-align: left;
    }

#content_symbols {
    font-size: 25px;
    color: #0CA0C6;
    }

.content_headings {
    font-size: 30px;
    margin-top: 25px;
    }

#get_started {
    margin-top: 70vh;
    margin-left: 55vw;
    position: fixed;
    color: white;
    font-family: 'Lobster', cursive;
    text-align: center;
    }

#golf_tracking {
    font-size: 40px;
}

#get_started_button {
    font: 25px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    border-radius: 10px;
    margin-top: 10px;
    box-shadow: 0 0 25px white;
    -o-box-shadow: 0 0 25px white;
    -moz-box-shadow: 0 0 25px white;
    -webkit-box-shadow: 0 0 25px white;
    -ms-box-shadow: 0 0 25px white;
    background: #0CA0C6;
    }

.whitespace {
    top: 100vh;
    position: relative;
    width: 100%;
    height: 110px;
    background: white;
    }

#features {
    margin: auto;
    position: relative;
    top: 100vh;
    color: #505050;
    width: 90vw;
    border: 1px solid #fff;
    border-radius: 1px;
    background: #f6f6f6;
    box-shadow: 0 0 2px rgba(50,50,50,0.5);
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    padding: 5px;
    text-align: center;
    }

#features div h1 {
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#features_symbols {
    font-size: 25px;
    color: #0CA0C6;
    }

#footer {
    top: 100vh;
    position: relative;
    width: 100%;
    height: 100px;
    background: #505050;
    border-radius: 1px;
    }

#footer a {
    text-align: center;
    color: white;
    font: 18px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#footer a:hover {
    color: #0CA0C6;
    text-decoration: none;
    }

#footer_table {
    margin: auto;
    }

#footer_table td {
    padding: 30px 10px 0px 10px;
    }

#overlay {
    position: fixed;
    width: 100%;
    height: 100%;
    background: #000;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    opacity: 0.8;
    z-index: 1000;
    }

.table {
        text-align: center;
        margin: 0 auto;
        font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
        color: #505050;
        border-radius: 5px;
        }

.table tbody tr td {
        border-top: none; }

#overlay_signIn_form {
        width: 30vw;
        height: 45vh;
        margin: 0 auto;
        position: absolute;
        top: 5vh;
        left: 35vw;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;

    }

#overlay_forgot_form {
        width: 30vw;
        height: 30vh;
        margin: 0 auto;
        position: absolute;
        top: 55vh;
        left: 35vw;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;
    }

#overlay_forgot_exit_button {
        width: 20px;
        line-height: 20px;
        background: #505050;
        color: white;
        text-align: center;
        font-size: 13px;
        font-weight: bold;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        position: absolute;
        top: -5px;
        right: -5px;
    }

#forgot_table {
        text-align: center;
        margin: 0 auto;
        margin-top: 15px;
        font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
        color: #505050;
        border-radius: 5px;
        }

#overlay_register_form {
        width: 30vw;
        height: 47vh;
        margin: 0 auto;
        position: absolute;
        top: 5vh;
        left: 35vw;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;
    }

.overlay_exit_button {
        width: 30px;
        line-height: 30px;
        background: #505050;
        color: white;
        text-align: center;
        font-size: 16px;
        font-weight: bold;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        position: absolute;
        top: -10px;
        right: -10px;
    }

#overlay_contact_form {
        width: 30vw;
        height: 20vh;
        margin: 0 auto;
        position: absolute;
        top: 5vh;
        left: 35vw;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        display: none;
        background: #0CA0C6;
        border-radius: 5px;
    }

#overlay_sent_form {
        width: 30vw;
        height: 40vh;
        margin: 0 auto;
        position: absolute;
        top: 5vh;
        left: 35vw;
        z-index: 1500;
        box-shadow: 0 0 25px white;
        -o-box-shadow: 0 0 25px white;
        -moz-box-shadow: 0 0 25px white;
        -webkit-box-shadow: 0 0 25px white;
        -ms-box-shadow: 0 0 25px white;
        background: #0CA0C6;
        border-radius: 5px;
    }

.logIn {
        font-size: 50px;
        font-family: 'Lobster', cursive;
        text-shadow: 0 0 50px white;
        -o-text-shadow: 0 0 50px white;
        -moz-text-shadow: 0 0 50px white;
        -webkit-text-shadow: 0 0 50px white;
        -ms-text-shadow: 0 0 50px white;
    }

#submit {
    background: white;
    border-radius: 5px;
    }

#forgot {
    position: relative;
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    color: white;
    text-decoration: none;
    }

.email {
    color: white;
    }

@media screen and (min-width: 1600px){

#get_started {
    margin-left: 65vw;
    margin-top: 80vh;
    }

#overlay_signIn_form {
    width: 20vw;
    height: 30vh;
    left: 40vw;
    }

#overlay_forgot_form {
    width: 20vw;
    height: 15vh;
    top: 40vh;
    left: 40vw;
    }

#overlay_register_form {
    width: 20vw;
    height: 32vh;
    top: 5vh;
    left: 40vw;
    }

#overlay_contact_form {
    width: 20vw;
    height: 15vh;
    top: 5vh;
    left: 40vw;
    }

#overlay_sent_form {
    width: 20vw;
    height: 15vh;
    top: 5vh;
    left: 40vw;
    }

}

@media screen and (min-width: 769px) and (max-width: 1100px) {

#get_started {
    margin-top: 70vh;
    margin-left: 50vw;
    }

#overlay_signIn_form {
    width: 40vw;
    height: 40vh;
    left: 30vw;
    }

#overlay_forgot_form {
    width: 40vw;
    height: 30vh;
    left: 30vw;
\t}

#overlay_register_form {
    width: 40vw;
    height: 42vh;
    left: 30vw;
    }

#forgot_table {
    margin-top: 30px;
    }

#overlay_contact_form {
\twidth: 40vw;
\theight: 20vh;
\tleft: 30vw;
\t}

#overlay_sent_form {
\twidth: 40vw;
\theight: 35vh;
\tleft: 30vw;
\t}
}

@media screen and (max-width: 768px){

#golf_photo {
    margin-top: 10vh;
    height: 45vh;
    width: 100vw;
    }

#get_started {
    margin-top: 65vh;
    margin-left: 1vw;
    color: #0CA0C6;
    }

#get_started_button {
    margin-top: 5vh;
    font: 25px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

#content {
    width: 100vw;
    }

#features {
    width: 100vw;
    }

#overlay_signIn_form {
    width: 80vw;
    height: 50vh;
    top: 5vh;
    right: 0;
    left: 0;
    }

#overlay_forgot_form {
\twidth: 80vw;
\theight: 30vh;
    top: 58vh;
    right: 0;
    left: 0;
    }

#overlay_register_form {
    width: 80vw;
\theight: 50vh;
    right: 0;
    left: 0;
    }

#overlay_contact_form {
\twidth: 80vw;
    height: 22vh;
    right: 0;
    left: 0;
    }

#overlay_sent_form {
\twidth: 80vw;
    height: 42vh;
    right: 0;
    left: 0;
    }

#forgot_table {
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

.table {
    font: 15px 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif;
    }

.logIn {
    font-size: 40px;
    }

}

</style>

<title>Sent</title>

</head>

<body>
<div id=\"overlay\"></div>

<div id=\"overlay_signIn_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">SignIn</h1>
                    <form action=\"";
        // line 539
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"POST\">
                        Username:<br>
                        <input type=\"text\" name=\"_username\">
                        <br>
                        Password:<br>
                        <input type=\"password\" name=\"_password\">
                        <br><br>
                        <input id=\"submit\" type=\"submit\" value=\"Submit\">
                    </form>
                </td>
            </tr>
\t    <tr>
\t\t<td>
\t\t\t<div><a href=\"#\" id=\"forgot\">Forgot Username or Password?</a></div>
\t\t</td>
\t\t</tr>
        </tbody>
    </table>
</div>

<div id=\"overlay_register_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">Register</h1>
                    <form action=\"";
        // line 566
        echo $this->env->getExtension('routing')->getPath("register");
        echo "\" method=\"POST\">
                        New Username:<br>
                        <input type=\"text\" name=\"_username\">
                        <br>
                        New Password:<br>
                        <input type=\"password\" name=\"_password\">
                        <br>
                        Email:<br>
                        <input type=\"email\" name=\"_email\">
                        <br><br>
                        <input id=\"submit\" type=\"submit\" value=\"Submit\">
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id=\"overlay_forgot_form\">
    <div id=\"overlay_forgot_exit_button\">X</div>
    <table id=\"forgot_table\">
        <tbody>
            <tr>
                <td>
                    <form action=\"";
        // line 590
        echo $this->env->getExtension('routing')->getPath("forgot_user_credentials");
        echo "\" method=\"POST\">
\t\t\t            Enter email:<br>
                        <input type=\"email\" name=\"email\">
                        <br><br>
                        <div class=\"email\">For security purposes, you will be assigned a temporary password.</div>
                        <br>
                        <input id=\"submit\" type=\"submit\" value=\"Retrieve\">
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id=\"overlay_contact_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">Contact Us</h1>
                    <form method=\"POST\">
                        <div class=\"email\">Email: tim.zabawa@golftracker.com</div><br>
                        <br>
                    </form>
                </td>
           </tr>
        </tbody>
    </table>
</div>

";
        // line 621
        if (((isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")) == false)) {
            // line 622
            echo "<div id=\"overlay_sent_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">Failure</h1>
                    <form method=\"POST\">
                        <div class=\"email\">We could not find a user registered to the entered email, please try again!</div><br>
                        <br>
                    </form>
                </td>
           </tr>
        </tbody>
    </table>
</div>
";
        } else {
            // line 639
            echo "<div id=\"overlay_sent_form\">
    <div class=\"overlay_exit_button\">X</div>
    <table class=\"table\">
        <tbody>
            <tr>
                <td>
                    <h1 class=\"logIn\">Success</h1>
                    <form method=\"POST\">
                        <div class=\"email\">Please check your email box to view your username and temporary password!</div><br>
                        <div class=\"email\">* check spam folder</div><br>
                        <div class=\"email\">* temporary password is case-sensitive</div><br>
                        <br>
                    </form>
                </td>
           </tr>
        </tbody>
    </table>
</div>
";
        }
        // line 658
        echo "
<div id=\"container\">

<header>
<nav id=\"nav_bar\" class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a id=\"golf_tracker\" class=\"navbar-brand\" href=\"#\">Golf<b>Tracker</b></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
        <li><a id=\"point_1\" href=\"#\">How It Works</a></li>
        <li><a id=\"point_2\" href=\"#\">Features</a></li>
      </ul>
      <ul class=\"nav navbar-nav navbar-right\">
        <li><a id=\"sign_in\" href=\"#\">Sign In</a></li>
        <li><a id=\"register\" href=\"#\">Register</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>


<div id=\"banner\">
    <img id=\"golf_photo\" src=\"https://farm8.staticflickr.com/7367/9321187216_13a434cf04_o_d.jpg\"/>
</div>

<table id=\"get_started\">
    <tr>
        <td>
            <h1 id=\"golf_tracking\">Golf Tracking Made Simple</h1>
        </td>
    </tr>
    <tr>
        <td>
            <button id=\"get_started_button\" type=\"button\" class=\"btn btn-info\">Get Started</button>
        </td>
    </tr>
</table>

<div id=\"1\" class=\"whitespace\"></div>

<div id=\"content\">
    <h1>How It Works</h1>
    <div class=\"row Grid1\">
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Get started.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-refresh\" aria-hidden=\"true\"></span>
            <p>After you create your account, you will be redirected to your home page. Notice the following fields:</p>
            <p>1). Your golf average.</p>
            <p>2). Your golf handicap.</p>
            <p>3). 5 scorecards providing the course you played at. the date, and the scores you recorded.</p>
            <p>4). A navigation bar field for entering a new score.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Enter score.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>
            <p>Upon clicking on the appropriate field to enter a new score, you will be taken to the score page.</p>
            <p>Here you will be able to enter your round. Fill out the following fields:</p>
            <p>1). The golf course name (just the name).</p>
            <p>2). The date on which you played.</p>
            <p>3). The par of each hole.</p>
            <p>4). The score you shot on each hole.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">View Results.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-signal\" aria-hidden=\"true\"></span>
            <p>Once you have entered a new score, you will be taken back to your home page.</p>
            <p>Notice that your golf average and first scorecard has been automatically updated.</p>
            <p>1). GolfTracker will automatically update your total golf average.</p>
            <p>2). GolfTracker will display the 5 latest scorecards you have entered.</p>
            <p>3). GolfTracker will automatically update your golf handicap after 5 scorecards have been submitted.</p>
        </div>
        <div class=\"col-xs-12 col-md-3\">
            <h1 class=\"content_headings\">Have fun.</h1>
            <span id=\"content_symbols\" class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\"></span>
            <p>The goal of GolfTracker is to provide an easy way for an average golfer to keep track of their average and handicap.</p>
            <p>We take care of all of the calculating. All you have to fill out your scorecards.</p>
            <p>Good luck and enjoy!</p>
        </div>
    </div>
</div>

<div class=\"whitespace\"></div>

<div id=\"features\">
<h1 id=\"features_heading\">Features</h1>
<div class=\"row Grid1\">
        <div class=\"col-xs-12 col-md-4\">
            <h1>Automated tracking of your golf average.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-signal\" aria-hidden=\"true\"></span>
        </div>
        <div class=\"col-xs-12 col-md-4\">
            <h1>Automated tracking of your golfhandicap.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-plus-sign\" aria-hidden=\"true\"></span>
        </div>
        <div class=\"col-xs-12 col-md-4\">
            <h1>List of the 5 most recent golf courses you played at and the scores you recorded.</h1>
            <span id=\"features_symbols\" class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></span>
        </div>
</div>
</div>

<div id=\"2\" class=\"whitespace\"></div>

<footer>
    <div id=\"footer\">
        <table id=\"footer_table\">
            <tr>
                <td>
                    <a id=\"contact\" href=\"#\">Contact Us</a>
                </td>
            </tr>
        </table>
    </div>
</footer>

</div>
</body>

<script type=\"text/javascript\">
\$(function() {

    \$(\"#point_1\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":\$(\"#1\").offset().top}, 1000);
    return false;
    });

    \$(\"#point_2\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":\$(\"#2\").offset().top}, 1000);
    return false;
    });

    \$(\"#golf_tracker\").on(\"click\", function() {
    \$(\"html body\").animate({\"scrollTop\":0}, 1000);
    return false;
    });

    \$(\"#sign_in\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_signIn_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\"#register\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_register_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\"#get_started_button\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_register_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\"#forgot\").on(\"click\", function () {
    \$(\"#overlay_forgot_form\").fadeIn(\"slow\");
    \$(\"#overlay_forgot_exit_button\").fadeIn(\"slow\");
    });

    \$(\".overlay_exit_button\").on(\"click\", function () {
    \$(\"#overlay\").fadeOut(\"fast\");
    \$(\"#overlay_signIn_form\").fadeOut(\"fast\");
    \$(\"#overlay_register_form\").fadeOut(\"fast\");
    \$(\"#overlay_contact_form\").fadeOut(\"fast\");
    \$(\"#overlay_sent_form\").fadeOut(\"fast\");
    \$(\".overlay_exit_button\").fadeOut(\"fast\");
    });

    \$(\"#overlay_forgot_exit_button\").on(\"click\", function () {
    \$(\"#overlay_forgot_form\").fadeOut(\"fast\");
    \$(\"#overlay_forgot_exit_button\").fadeOut(\"fast\");
    });

    \$(\"#contact\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_contact_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

    \$(\"#contact\").on(\"click\", function () {
    \$(\"#overlay\").fadeIn(\"slow\");
    \$(\"#overlay_contact_form\").fadeIn(\"slow\");
    \$(\".overlay_exit_button\").fadeIn(\"slow\");
    });

});

</script>

</html>



";
        
        $__internal_2f2b2a36091e8db8b6aa3c0a291dd47e8aee9b40589e765acd7651c85391652c->leave($__internal_2f2b2a36091e8db8b6aa3c0a291dd47e8aee9b40589e765acd7651c85391652c_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:Default:sent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  704 => 658,  683 => 639,  664 => 622,  662 => 621,  628 => 590,  601 => 566,  571 => 539,  37 => 8,  33 => 7,  29 => 6,  22 => 1,);
    }
}
