<?php

/* golfBundle:default:register.html.twig */
class __TwigTemplate_106e758b16486753502935fb56d59a19721178234d0015413d2368d5cb71c4e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c97913ebb31dddd259021f7a7355e888811a92a5460a4e378a9c9f9240b000f4 = $this->env->getExtension("native_profiler");
        $__internal_c97913ebb31dddd259021f7a7355e888811a92a5460a4e378a9c9f9240b000f4->enter($__internal_c97913ebb31dddd259021f7a7355e888811a92a5460a4e378a9c9f9240b000f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "golfBundle:default:register.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <title>Register</title>
</head>
<body>
";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
</body>
</html>";
        
        $__internal_c97913ebb31dddd259021f7a7355e888811a92a5460a4e378a9c9f9240b000f4->leave($__internal_c97913ebb31dddd259021f7a7355e888811a92a5460a4e378a9c9f9240b000f4_prof);

    }

    public function getTemplateName()
    {
        return "golfBundle:default:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  22 => 1,);
    }
}
