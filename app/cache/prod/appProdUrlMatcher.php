<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // user_root
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'user_root');
            }

            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::rootAction',  '_route' => 'user_root',);
        }

        // user_homepage
        if ($pathinfo === '/homepage') {
            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::homepageAction',  '_route' => 'user_homepage',);
        }

        // scorecard
        if ($pathinfo === '/scorecard') {
            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::scorecardAction',  '_route' => 'scorecard',);
        }

        // register
        if ($pathinfo === '/register') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_register;
            }

            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::registerAction',  '_route' => 'register',);
        }
        not_register:

        if (0 === strpos($pathinfo, '/login')) {
            // login_fail
            if ($pathinfo === '/login_fail') {
                return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::login_failAction',  '_route' => 'login_fail',);
            }

            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::loginAction',  '_route' => 'login',);
            }

        }

        // forgot_user_credentials
        if ($pathinfo === '/forgot_credentials') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_forgot_user_credentials;
            }

            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::forgotAction',  '_route' => 'forgot_user_credentials',);
        }
        not_forgot_user_credentials:

        // new_password
        if ($pathinfo === '/new_password') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_new_password;
            }

            return array (  '_controller' => 'golfBundle\\Controller\\DefaultController::new_passwordAction',  '_route' => 'new_password',);
        }
        not_new_password:

        if (0 === strpos($pathinfo, '/log')) {
            // login_check
            if ($pathinfo === '/login_check') {
                return array('_route' => 'login_check');
            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
