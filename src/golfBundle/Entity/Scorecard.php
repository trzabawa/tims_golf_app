<?php

namespace golfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @ORM\Entity(repositoryClass="golfBundle\Entity\ScorecardRepository")
 * @ORM\Table(name="scorecard")
 */

class Scorecard 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    protected $timestamp;
    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $user;
    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\NotBlank()
     */
    protected $course;
    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank()
     */
    protected $date;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_3;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_4;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_5;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_6;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_7;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_8;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_9;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_10;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_11;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_12;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_13;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_14;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_15;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_16;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_17;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $par_18;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_1;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_2;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_3;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_4;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_5;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_6;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_7;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_8;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_9;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_10;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_11;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_12;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_13;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_14;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_15;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_16;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_17;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $score_18;
    
    public function __construct(){$this->timestamp = new \DateTime();}
    public function getId(){return $this->id;}
    public function setUser($user){$this->user = $user; return $this;}
    public function getUser(){return $this->user;}
    public function setPar1($par1){$this->par_1 = $par1;return $this;}
    public function getPar1(){return $this->par_1;}
    public function setPar2($par2){$this->par_2 = $par2; return $this;}
    public function getPar2(){return $this->par_2;}
    public function setPar3($par3){$this->par_3 = $par3; return $this;}
    public function getPar3(){return $this->par_3;}
    public function setPar4($par4){$this->par_4 = $par4; return $this;}
    public function getPar4(){return $this->par_4;}
    public function setPar5($par5){$this->par_5 = $par5; return $this;}
    public function getPar5(){return $this->par_5;}
    public function setPar6($par6){$this->par_6 = $par6; return $this;}
    public function getPar6(){return $this->par_6;}
    public function setPar7($par7){$this->par_7 = $par7; return $this;}
    public function getPar7(){return $this->par_7;}
    public function setPar8($par8){$this->par_8 = $par8; return $this;}
    public function getPar8(){return $this->par_8;}
    public function setPar9($par9){$this->par_9 = $par9; return $this;}
    public function getPar9(){return $this->par_9;}
    public function setPar10($par10){$this->par_10 = $par10; return $this;}
    public function getPar10(){return $this->par_10;}
    public function setPar11($par11){$this->par_11 = $par11; return $this;}
    public function getPar11(){return $this->par_11;}
    public function setPar12($par12){$this->par_12 = $par12; return $this;}
    public function getPar12(){return $this->par_12;}
    public function setPar13($par13){$this->par_13 = $par13; return $this;}
    public function getPar13(){return $this->par_13;}
    public function setPar14($par14){$this->par_14 = $par14; return $this;}
    public function getPar14(){return $this->par_14;}
    public function setPar15($par15){$this->par_15 = $par15; return $this;}
    public function getPar15(){return $this->par_15;}
    public function setPar16($par16){$this->par_16 = $par16; return $this;}
    public function getPar16(){return $this->par_16;}
    public function setPar17($par17){$this->par_17 = $par17; return $this;}
    public function getPar17(){return $this->par_17;}
    public function setPar18($par18){$this->par_18 = $par18; return $this;}
    public function getPar18(){return $this->par_18;}
    public function setScore1($score1){$this->score_1 = $score1; return $this;}
    public function getScore1(){return $this->score_1;}
    public function setScore2($score2){$this->score_2 = $score2; return $this;}
    public function getScore2(){return $this->score_2;}
    public function setScore3($score3){$this->score_3 = $score3; return $this;}
    public function getScore3(){return $this->score_3;}
    public function setScore4($score4){$this->score_4 = $score4; return $this;}
    public function getScore4(){return $this->score_4;}
    public function setScore5($score5){$this->score_5 = $score5; return $this;}
    public function getScore5(){return $this->score_5;}
    public function setScore6($score6){$this->score_6 = $score6; return $this;}
    public function getScore6(){return $this->score_6;}
    public function setScore7($score7){$this->score_7 = $score7; return $this;}
    public function getScore7(){return $this->score_7;}
    public function setScore8($score8){$this->score_8 = $score8;return $this;}
    public function getScore8(){return $this->score_8;}
    public function setScore9($score9){$this->score_9 = $score9; return $this;}
    public function getScore9(){return $this->score_9;}
    public function setScore10($score10){$this->score_10 = $score10; return $this;}
    public function getScore10(){return $this->score_10;}
    public function setScore11($score11){$this->score_11 = $score11; return $this;}
    public function getScore11(){return $this->score_11;}
    public function setScore12($score12){$this->score_12 = $score12; return $this;}
    public function getScore12(){return $this->score_12;}
    public function setScore13($score13){$this->score_13 = $score13; return $this;}
    public function getScore13(){return $this->score_13;}
    public function setScore14($score14){$this->score_14 = $score14; return $this;}
    public function getScore14(){return $this->score_14;}
    public function setScore15($score15){$this->score_15 = $score15; return $this;}
    public function getScore15(){return $this->score_15;}
    public function setScore16($score16){$this->score_16 = $score16; return $this;}
    public function getScore16(){return $this->score_16;}
    public function setScore17($score17){$this->score_17 = $score17; return $this;}
    public function getScore17(){return $this->score_17;}
    public function setScore18($score18){$this->score_18 = $score18; return $this;}
    public function getScore18(){return $this->score_18;}
    public function setCourse($course){$this->course = $course; return $this;}
    public function getCourse(){return $this->course;}
    public function setDate($date){$this->date = $date; return $this;}
    public function getDate(){return $this->date;}
}
