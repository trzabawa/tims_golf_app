<?php

namespace golfBundle\Controller;

use golfBundle\Entity\User;

use golfBundle\Entity\Scorecard;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Symfony\Component\Security\Core\Util\SecureRandom;

use Mailgun\Mailgun;

use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function rootAction()
    {
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        return $this->redirectToRoute('user_homepage');
        }
        return $this->render('golfBundle:Default:root.html.twig');
    }

    public function registerAction(Request $request)
    {
        $request = $this->get('request');
        $username = $request->request->get('_username');
        $password = $request->request->get('_password');
        $email = $request->request->get('_email');
        $user = new User();
        $user->setusername($username);
        $user->setpassword($password);
        $user->setemail($email);
        $plainPassword = $request->request->get('_password');
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $plainPassword);
        $user->setpassword($encoded);
        $repository = $this->getDoctrine()
        ->getRepository('golfBundle:User');
        $user_found_username = $repository->findOneByUsername($user->getusername());
        $user_found_email = $repository->findOneByEmail($user->getemail());
        if ($password=="" || $username=="" || $email=="") {
        $response=array("responseCode"=>400);
        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
        }
        elseif (!$user_found_username && !$user_found_email) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $token = new UsernamePasswordToken($user, null, 'default', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_default',serialize($token));
        $url = 'http://localhost:8080/app.php/homepage';
        $response=array("responseCode"=>300, "url"=>$url);
        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
        }
        else {
        $response=array("responseCode"=>200);
        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
        }
    }

    public function homepageaction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $username = $user->getusername();
        $repository = $this->getDoctrine()
        ->getRepository('golfBundle:Scorecard');
        $query = $repository->createQueryBuilder('scorecards')
        ->where('scorecards.user = :username')
        ->setParameter('username', $username)
        ->orderBy('scorecards.timestamp', 'DESC')
        ->getQuery();
        $user_scorecard = $query->getResult();

        function par_front9($object) {
            $sum = 0;
            $number = 1;
            for ($x = 0; $x <= 8; $x++)
                {
                    $par = 'getPar' . $number;
                    $sum += $object->$par();
                    $number += 1;
                }
            return $sum;
        }
        function par_back9($object) {
            $sum = 0;
            $number = 10;
            for ($x = 9; $x <= 17; $x++)
                {
                    $par = 'getPar' . $number;
                    $sum += $object->$par();
                    $number += 1;
                }
            return $sum;
        }
        function score_front9($object) {
            $sum = 0;
            $number = 1;
            for ($x = 0; $x <= 8; $x++)
                {
                    $par = 'getScore' . $number;
                    $sum += $object->$par();
                    $number += 1;
                }
            return $sum;
        }
        function score_back9($object) {
            $sum = 0;
            $number = 10;
            for ($x = 9; $x <= 17; $x++)
                {
                    $par = 'getScore' . $number;
                    $sum += $object->$par();
                    $number += 1;
                }
            return $sum;
        }

        function score_average($object) {
             $length = 0;
             $average = 0;
             $number = 1;
             $key = 0;
             while (count($object) != $length) {
                for ($x = 0; $x <= 17; $x++)
                {
                    $score = 'getScore' . $number;
                    $average += $object[$key]->$score();
                    $number++;
                }
                $length++;
                $key++;
                $number = 1;
              }

             if (count($object) == 0) {
             return 0;
             }
             else {
             return $average / count($object);
             }
        }

        $user_score_average = score_average($user_scorecard);
        $par_front9_total = array();
        $par_back9_total = array();
        $score_front9_total = array();
        $score_back9_total = array();
        if ($user_scorecard != null) {
            $length = 0;
            $key = 0;
            for($x = 0; $x <= 4; $x++) {
                if (count($user_scorecard) != $length) {
                $par_front9_total[$key] = par_front9($user_scorecard[$key]);
                $par_back9_total[$key] = par_back9($user_scorecard[$key]);
                $score_front9_total[$key] = score_front9($user_scorecard[$key]);
                $score_back9_total[$key] = score_back9($user_scorecard[$key]);
                $key += 1;
                $length += 1;
                }
            }
        }

        return $this->render('golfBundle:Default:homepage.html.twig',array('username'=>$username,'user_scorecard'=>$user_scorecard,'par_front9_total'=>$par_front9_total,'par_back9_total'=>$par_back9_total,'score_front9_total'=>$score_front9_total, 'score_back9_total'=>$score_back9_total,'user_score_average'=>$user_score_average));
    }

    public function login_failaction()
    {
        $response=array("responseCode"=>300);
        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }


    public function loginaction()
    {
        $url = 'http://localhost:8080/app.php/homepage';
        $response=array("responseCode"=>200, "url"=>$url);
        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }

    public function scorecardaction(Request $request)
    {
        if ($request->getmethod()=='POST'){
        $username = $this->getUser()->getusername();
        $scorecard = new Scorecard();
        $scorecard->setUser($username);
        $scorecard->setCourse($request->get('course'));
        $scorecard->setDate($request->get('date'));
        $scorecard->setPar1($request->get('par1'));
        $scorecard->setPar2($request->get('par2'));
        $scorecard->setPar3($request->get('par3'));
        $scorecard->setPar4($request->get('par4'));
        $scorecard->setPar5($request->get('par5'));
        $scorecard->setPar6($request->get('par6'));
        $scorecard->setPar7($request->get('par7'));
        $scorecard->setPar8($request->get('par8'));
        $scorecard->setPar9($request->get('par9'));
        $scorecard->setPar10($request->get('par10'));
        $scorecard->setPar11($request->get('par11'));
        $scorecard->setPar12($request->get('par12'));
        $scorecard->setPar13($request->get('par13'));
        $scorecard->setPar14($request->get('par14'));
        $scorecard->setPar15($request->get('par15'));
        $scorecard->setPar16($request->get('par16'));
        $scorecard->setPar17($request->get('par17'));
        $scorecard->setPar18($request->get('par18'));
        $scorecard->setScore1($request->get('score1'));
        $scorecard->setScore2($request->get('score2'));
        $scorecard->setScore3($request->get('score3'));
        $scorecard->setScore4($request->get('score4'));
        $scorecard->setScore5($request->get('score5'));
        $scorecard->setScore6($request->get('score6'));
        $scorecard->setScore7($request->get('score7'));
        $scorecard->setScore8($request->get('score8'));
        $scorecard->setScore9($request->get('score9'));
        $scorecard->setScore10($request->get('score10'));
        $scorecard->setScore11($request->get('score11'));
        $scorecard->setScore12($request->get('score12'));
        $scorecard->setScore13($request->get('score13'));
        $scorecard->setScore14($request->get('score14'));
        $scorecard->setScore15($request->get('score15'));
        $scorecard->setScore16($request->get('score16'));
        $scorecard->setScore17($request->get('score17'));
        $scorecard->setScore18($request->get('score18'));
        $repository = $this->getDoctrine()
        ->getRepository('golfBundle:Scorecard');
        $em = $this->getDoctrine()->getManager();
        $em->persist($scorecard);
        $em->flush();
        return $this->redirectToRoute('user_homepage');
        }
        else {
        return $this->redirectToRoute('user_homepage');
        }
    }

    public function new_passwordaction(){
        $request = $this->get('request');
        $password=$request->request->get('password');
        if($password!=""){//if the user has written his new password
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $password);
            $user->setpassword($encoded);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $response=array("responseCode"=>200);
        }
        else{
            $response=array("responseCode"=>400);
        }

        $return=json_encode($response);//jscon encode the array
        return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }

    public function forgotaction(Request $request)
    {
        $request = $this->get('request');
        $email=$request->request->get('email');
        if($email==""){
        $response=array("responseCode"=>400);
        }
        else {
        $user = new User();
        $user->setemail($email);
        $repository = $this->getDoctrine()
        ->getRepository('golfBundle:User');
        $user_found = $repository->findOneByEmail($user->getemail());

         if (!$user_found) {
         $success = 1;
         $response=array("responseCode"=>200, "success"=>$success);
         }

         else {
         $mgClient = new Mailgun('key-55c438d4f58daec65e6e37742dd2c63e');
         $domain = "golftrackersolutions.com";
         $recipient = $user_found->getemail();
         $username = $user_found->getusername();
         $length = 10;
         $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
         $password = $randomString;
         $users_username = "Your Username is: " . $username . ".";
         $case_sensitive = " REMEMBER that the temporary password is case-sensitive.";
         $users_direction = " Once you have signed in, to select a new password, please click on your username displayed on your homepage.";
         $users_username_and_password = $users_username . " Your temporary Password is: " . $password . "." . $case_sensitive . $users_direction;
         $mgClient->sendMessage($domain, array(
         'from'    => 'tzabawa@golftrackersolutions',
         'to'      => $recipient,
         'subject' => 'Username & Password for golftrackersolutions.com',
         'text'    => $users_username_and_password
         ));
         $encoder = $this->container->get('security.password_encoder');
         $encoded = $encoder->encodePassword($user, $password);
         $user_found->setpassword($encoded);
         $em = $this->getDoctrine()->getManager();
         $em->flush();
         $success = 2;
         $response=array("responseCode"=>200, "success"=>$success);
         }

        }
         $return=json_encode($response);//jscon encode the array
         return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type

    }

}






